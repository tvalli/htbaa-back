package fr.capgemini.codingdojo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.capgemini.codingdojo.entity.User;
import fr.capgemini.codingdojo.repository.UserRepository;

@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/users")
	public List<User> getUsers() {
		return this.userRepository.findAll();
	}

	@PostMapping("/users")
    public void addUser(@RequestBody User user) {
        userRepository.save(user);
    }
	
}
