package fr.capgemini.codingdojo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LostController {
	
	@GetMapping("/lost")
	public String getLost() {
		return "lost";
	}

	
}
